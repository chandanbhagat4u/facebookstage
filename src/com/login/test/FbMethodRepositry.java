package com.login.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class FbMethodRepositry {
	WebDriver driver;

	public void fbLaunch() throws InterruptedException {
		// Dismiss the chorme browser notification
		ChromeOptions options = new ChromeOptions();
		//Check Aarush added in bit bucket.
		options.addArguments("--disable-notifications");
		// Open the Browser
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		Thread.sleep(2000);
		System.out.println("Browser open");
	}

	// Check the Select class on DOB in facebook page
	public void dobCheck() throws InterruptedException {
		WebElement date = driver.findElement(By.xpath("//select[@id='day']"));
		Select datedropdown = new Select(date);
		datedropdown.selectByVisibleText("29");
		Thread.sleep(1000);

		WebElement month = driver.findElement(By.xpath("//select[@id='month']"));
		Select monthdropdown = new Select(month);
		monthdropdown.selectByIndex(8);
		Thread.sleep(1000);

		WebElement year = driver.findElement(By.xpath("//select[@id='year']"));
		Select yeardropdown = new Select(year);
		yeardropdown.selectByValue("1986");
	}

	public void verifylogin() throws InterruptedException, AWTException, FindFailed {
		// Using java script executor
		/*
		 * WebElement uname = driver.findElement(By.name("email"));
		 * uname.sendKeys("cftester8@gmail.com"); WebElement pwd =
		 * driver.findElement(By.name("pass")); pwd.sendKeys("cf12345678");
		 */

		// Using Sikuli for username
		/*
		 * Screen screen = new Screen(); Thread.sleep(5000); Pattern username =
		 * new Pattern("./SikuliImage/FBEmail.png"); screen.wait(username, 10);
		 * screen.type("cftester8@gmail.com");
		 */

		// Using Action Class
		/*
		 * WebElement uname = driver.findElement(By.name("email")); Actions
		 * builder = new Actions(driver);
		 * builder.moveToElement(uname).click().sendKeys("cftester8@gmail.com").
		 * build().perform();
		 * 
		 * WebElement pwd = driver.findElement(By.name("pass")); Actions
		 * password = new Actions(driver);
		 * password.moveToElement(pwd).click().sendKeys("cf12345678").build().
		 * perform();
		 * 
		 * WebElement login =
		 * driver.findElement(By.xpath("//input[@value='Log In']")); Actions
		 * signin = new Actions(driver);
		 * signin.moveToElement(login).click().build().perform();
		 */

		// Using Java script executor
		Thread.sleep(5000);
		WebElement uname = driver.findElement(By.name("email"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].value='cftester8@gmail.com';", uname);

		WebElement pwd = driver.findElement(By.name("pass"));
		JavascriptExecutor pwdjs = (JavascriptExecutor) driver;
		pwdjs.executeScript("arguments[0].value='cf12345678';", pwd);

		WebElement login = driver.findElement(By.xpath("//input[@value='Log In']"));
		JavascriptExecutor jslogin = (JavascriptExecutor) driver;
		jslogin.executeScript("arguments[0].click()", login);

		/*
		 * WebElement login =
		 * driver.findElement(By.xpath("//input[@value='Log In']"));
		 * login.click(); System.out.println("Login Successfull");
		 * Thread.sleep(3000);
		 */

		// Now using robot class for login
		/*
		 * Robot robotObj = new Robot(); robotObj.keyPress(KeyEvent.VK_TAB);
		 * robotObj.keyRelease(KeyEvent.VK_ENTER);
		 * robotObj.keyPress(KeyEvent.VK_ENTER);
		 */

		System.out.println("Facebook login Sucessfull");

		String actualTitle = driver.getTitle(); // Get the actual page title
		System.out.println("The actual title is " + actualTitle);
		String ExpectedTitle = "Facebook1";

		if (ExpectedTitle.equals(actualTitle)) {
			System.out.println("Expected result is matched with Actula result - Passed");
		} else {
			System.out.println("Expected result is not matched with Actula result - Failed");
		}

	}

	public void searchfn() {
		WebElement searchfield = driver.findElement(By.xpath("//input[@class='_5eay']")); // "//input[@name='userName']"
		searchfield.click();
		searchfield.clear();
		searchfield.sendKeys("Kolkata");
		WebElement searchicon = driver.findElement(By.xpath("//i[@class='_585_']"));
		searchicon.click();
		System.out.println("Search is done.");
	}

	public void rightmenu() throws InterruptedException {

		WebElement nav = driver.findElement(By.xpath("//div[@id='userNavigationLabel']"));
		nav.click();
		Thread.sleep(3000);
		WebElement logout = driver.findElement(By.xpath("//li[@class='_54ni navSubmenu _6398 _64kz __MenuItem']"));
		logout.click();
		System.out.println("Logout Succesfully");
	}

	public void accSetting() throws InterruptedException, AWTException {
		WebElement accset = driver.findElement(By.id("userNavigationLabel"));
		accset.click();
		Thread.sleep(15000);
		WebElement sett = driver.findElement(By.xpath("//ul[@class='_54nf']//li[11]"));
		sett.click();
		Thread.sleep(5000);
		
		WebElement editName = driver.findElement(By.xpath("//ul[@class='uiList fbSettingsList fbSettingsListBorderTop fbSettingsListBorderBottom _4kg _4ks']//li[1]//a[1]//span[1]//span[1]"));
		Actions rightClick = new Actions(driver);
		WebElement rightClickElement= editName;
		rightClick.contextClick(rightClickElement).build().perform();
		//WebElement getCopyText = driver.findElement(By.cssSelector(".context-menu-icon-copy"));
		//String GetText = getCopyText.getText();
		//System.out.println(GetText);
		//rightClick.contextClick(editName).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.RETURN).click().build().perform();
	    Robot opNew = new Robot();
	    opNew.keyPress(KeyEvent.VK_DOWN);
	    opNew.keyRelease(KeyEvent.VK_DOWN);
	    opNew.keyPress(KeyEvent.VK_ENTER);
	    opNew.keyRelease(KeyEvent.VK_ENTER);
		
	}

	public void profile() throws InterruptedException, IOException {
		WebElement profilename = driver.findElement(By.xpath("//span[@class='_1vp5']"));
		profilename.click();
		Thread.sleep(5000);
		WebElement photo = driver.findElement(By.linkText("Photos"));
		Thread.sleep(10000);
		photo.click();
		System.out.println("Upload photo clicked");

		Thread.sleep(10000);
		WebElement addphoto = driver.findElement(By.linkText("Add Photos/Video"));
		addphoto.click();

		// Use AutoIT for photo upload
		Runtime.getRuntime().exec("./Driver/FlieFmFileUpload.exe");
		Thread.sleep(10000);

		driver.switchTo().activeElement();
		Thread.sleep(10000);
		WebElement post = driver.findElement(By.xpath("//button[@value='1']//span[contains(text(),'Post')]"));
		post.click();
		System.out.println("Photo uploaded Succesfull");

		/*
		 * Thread.sleep(1000); driver.switchTo().activeElement().
		 * sendKeys("C/Users/Public/Pictures/Sample Pictures/Tulips.jpg");
		 * Thread.sleep(1000);
		 */
	}

	public void openNotepad() throws InterruptedException, FindFailed {
		Screen screen = new Screen();
		Thread.sleep(5000);
		Pattern start = new Pattern("./SikuliImage/Start.png");
		screen.wait(start, 10);
		screen.click();
		Thread.sleep(1000);
		Pattern notepad = new Pattern("./SikuliImage/Notepad.png");
		screen.wait(notepad, 10);
		screen.doubleClick();

	}

}
